package entity;

/**
 * @Author: huangjc
 * @Date: Created in 2018/12/28 17:09
 */
public class Person {
    public void say() {
        System.out.println("hello, my name is liming-1");
        System.out.println("hello, my name is liming-2");
        System.out.println("hello, my name is liming-3");
    }

    public void eat(String zhushi, String meat) {
        System.out.println("i like " + zhushi + " and " + meat);
    }
}
